package com.example.picsum

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.WorkerThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bignerdranch.android.photogallery.PhotoGalleryViewModel
import com.squareup.picasso.Picasso
import java.io.FileOutputStream
import kotlin.concurrent.thread


private const val TAG = "PhotoGalleryFragment"

open class PhotoGalleryFragment : CommonFragment() {

    private lateinit var photoRecyclerView: RecyclerView
    private lateinit var photoGalleryViewModel: PhotoGalleryViewModel
    private lateinit var liked: List<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        photoGalleryViewModel = ViewModelProvider(requireActivity()).get(PhotoGalleryViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_photo_gallery, container, false)
        photoRecyclerView = view.findViewById(R.id.photo_recycler_view)
        photoRecyclerView.layoutManager = GridLayoutManager(context, 1)
        return view
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        liked = getAllPictures().map {
            it.name.replace(".png", "")
        }
        photoGalleryViewModel.galleryItemLiveData.observe(
            viewLifecycleOwner,
            Observer { galleryItems ->
                Log.d(TAG, "---------Have gallery items from ViewModel $galleryItems")
                photoRecyclerView.adapter = PhotoAdapter(galleryItems)
            })
    }

    private inner class PhotoAdapter(private val galleryItems: List<GalleryItem>) : RecyclerView.Adapter<ViewHolder>() {

        var lastGaleryItemsSize = 0


        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ViewHolder {
            val view = layoutInflater.inflate(R.layout.list_item_gallery, parent, false) as View
            return ViewHolder(view)
        }

        override fun getItemCount(): Int = galleryItems.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val galleryItem = galleryItems[position]


            if(liked.contains(galleryItem.id)){
                holder.likeImageButton.setImageResource(R.drawable.star1)
            }else{
                holder.likeImageButton.setImageResource(R.drawable.star0)
            }

            holder.likeImageButton.setOnClickListener {
                holder.likeImageButton.setImageResource(R.drawable.star1)

                thread {
                    val bitmap = (holder.image.drawable as BitmapDrawable).bitmap
                    context?.let { saveImage(it, bitmap, galleryItem.id + ".png") }
                }
            }

            if(galleryItems.size - 5 <= position && lastGaleryItemsSize != galleryItems.size){
                Log.i(TAG, "add new " + galleryItems.size)
                photoGalleryViewModel.getNew()
                lastGaleryItemsSize = galleryItems.size
            }

            Picasso.get().load("https://picsum.photos/id/" + galleryItem.id + "/" + 600)
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                    .fit()
                .into(holder.image)

        }
    }

    companion object {
        fun newInstance() = PhotoGalleryFragment()
    }

    @WorkerThread
    fun saveImage(context: Context, b: Bitmap, imageName: String?) {

        val foStream: FileOutputStream
        try {
            foStream = context.openFileOutput(imageName, Context.MODE_PRIVATE)
            b.compress(Bitmap.CompressFormat.PNG, 100, foStream)
            foStream.close()
        } catch (e: Exception) {
            Log.d("saveImage", "Exception 2, Something went wrong!")
            e.printStackTrace()
        }
    }


}