package com.example.picsum

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

private const val TAG = "PhotoGalleryActivity"

class PhotoGalleryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_gallery)

        val isFragmentContainerEmpty:Boolean = savedInstanceState == null
        //вставляем фрагмент
        if (isFragmentContainerEmpty) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, PhotoGalleryFragment.newInstance())
                .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.getItemId()){
            R.id.all -> {
                Log.i(TAG, "all")
                replaceFragment(PhotoGalleryFragment.newInstance())
                return true
            }
            R.id.liked -> {
                Log.i(TAG, "liked")
                replaceFragment(LikedFragment.newInstance())
                return true
            }
        }
        return onOptionsItemSelected(item)
    }

    //todo add single top
    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()
    }


}