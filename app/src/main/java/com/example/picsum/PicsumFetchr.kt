package com.example.picsum

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.picsum.picsum.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val TAG = "FlickrFetchr"

class PicsumFetchr {

    private val apiInterface: ApiInterface
    init {
        apiInterface = ApiInterface.create()
    }

    fun fetchFirstPhotos(): MutableLiveData<MutableList<GalleryItem>> {
        val responseLiveData: MutableLiveData<MutableList<GalleryItem>> = MutableLiveData()
        val flickrRequest = apiInterface.getGaleryItemPage(1)

        flickrRequest.enqueue(object : Callback<MutableList<GalleryItem>> {

            override fun onFailure(call: Call<MutableList<GalleryItem>>, t: Throwable) {
                Log.e(TAG, "Failed to fetch photos", t)
            }

            override fun onResponse(call: Call<MutableList<GalleryItem>>, response: Response<MutableList<GalleryItem>>) {
                Log.d(TAG, "Response received")

                val galleryItems: MutableList<GalleryItem> = response.body()
                    ?: mutableListOf()

                responseLiveData.value = galleryItems

            }
        })
        return responseLiveData
    }

    fun fetchPhotos(numList:Int , list: MutableLiveData<MutableList<GalleryItem>> ): MutableLiveData<MutableList<GalleryItem>> {

        val flickrRequest = apiInterface.getGaleryItemPage(numList)

        flickrRequest.enqueue(object : Callback<MutableList<GalleryItem>> {

            override fun onFailure(call: Call<MutableList<GalleryItem>>, t: Throwable) {
                Log.e(TAG, "Failed to fetch photos", t)
            }

            override fun onResponse(call: Call<MutableList<GalleryItem>>, response: Response<MutableList<GalleryItem>>) {
                Log.d(TAG, "Response received")

                val galleryItems: MutableList<GalleryItem> = response.body()
                        ?: mutableListOf()

                list.value?.addAll(galleryItems)

            }

        })
        return list
    }
}