package com.example.picsum


import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.File

private const val TAG = "LikedFragment"

class LikedFragment : CommonFragment() {

    private lateinit var photoRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view = inflater.inflate(R.layout.fragment_photo_gallery, container, false)
        photoRecyclerView = view.findViewById(R.id.photo_recycler_view)
        photoRecyclerView.layoutManager = GridLayoutManager(context, 1)

       return view

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        photoRecyclerView.adapter = PhotoAdapter(getAllPictures())

    }




    private inner class PhotoAdapter(private val galleryItems: List<File>) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ViewHolder {
            val view = layoutInflater.inflate(R.layout.list_item_gallery, parent, false) as View
            return ViewHolder(view)
        }

        override fun getItemCount(): Int = galleryItems.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val galleryItem = galleryItems[position]
            holder.likeImageButton.visibility = View.INVISIBLE

            if(galleryItem.exists()){
                val myBitmap = BitmapFactory.decodeFile(galleryItem.absolutePath)
                holder.image.setImageBitmap(myBitmap)
            }

        }
    }

    companion object {
        fun newInstance() = LikedFragment()
    }

}