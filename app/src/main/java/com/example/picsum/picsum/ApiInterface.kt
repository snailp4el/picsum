package com.example.picsum.picsum


import com.example.picsum.GalleryItem
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("v2/list?limit=10&")
    fun getGaleryItemPage(@Query("page") pageSize:Int) : Call<MutableList<GalleryItem>>

    companion object {

        var BASE_URL = "https://picsum.photos/"

        fun create() : ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }
}