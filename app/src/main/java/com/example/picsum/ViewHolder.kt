package com.example.picsum

import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val image: ImageView
    val likeImageButton: ImageButton

    init {
        image = view.findViewById(R.id.image) as ImageView
        likeImageButton = view.findViewById(R.id.like) as ImageButton
    }
}