package com.example.picsum

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import java.io.File

open class CommonFragment : Fragment() {

    protected fun getAllPictures(): List<File> {
        val photoPath = context?.filesDir?.path ?: "/data/user/0/com.example.picsum/files"
        val directory = File(photoPath)
        val files = directory.listFiles().filter { it.name.contains(".png") }.toList()
        return files
    }

}