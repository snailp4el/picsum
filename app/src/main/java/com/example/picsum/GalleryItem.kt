package com.example.picsum

import com.google.gson.annotations.SerializedName

data class GalleryItem(
    var id: String = "",
    var url: String = ""
)