package com.example.picsum

import android.content.Context
import android.util.AttributeSet


class SquareImageView : androidx.appcompat.widget.AppCompatImageView {
    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?,
                defStyle: Int) : super(context!!, attrs, defStyle) {
    }


    override fun onMeasure(width: Int, height: Int) {
        super.onMeasure(width, height)
        val measuredWidth: Int = getMeasuredWidth()
        val measuredHeight: Int = getMeasuredHeight()
        if (measuredWidth > measuredHeight) {
            setMeasuredDimension(measuredHeight, measuredHeight)
        } else {
            setMeasuredDimension(measuredWidth, measuredWidth)
        }
    }
}