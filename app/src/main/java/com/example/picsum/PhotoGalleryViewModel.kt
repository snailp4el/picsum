package com.bignerdranch.android.photogallery

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.picsum.GalleryItem
import com.example.picsum.PicsumFetchr

class PhotoGalleryViewModel : ViewModel() {

    var galleryItemLiveData: MutableLiveData<MutableList<GalleryItem>> = MutableLiveData()
    var pagesLoadaded = 1

    init {
        galleryItemLiveData = PicsumFetchr().fetchFirstPhotos()
    }

    fun getNew(){
        pagesLoadaded++
        PicsumFetchr().fetchPhotos(pagesLoadaded, galleryItemLiveData)
    }

}